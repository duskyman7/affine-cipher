#include <sstream>
#include <iostream>
#include <vector>

const int MODULO = 256;

// iba pre niektore 2x2 matice
std::vector<std::vector<int>> inverse_matrix(std::vector<std::vector<int>> &a){
    return {
            {-a[1][1],a[0][1]},
            {a[1][0],-a[0][0]}
    };
}

// iba pre matice 2x2
std::vector<std::vector<int>> multiply_matrices(
        std::vector<std::vector<int>> &a,
        std::vector<std::vector<int>> &b)
{
    return {
            {a[0][0] * b[0][0] + a[0][1]*b[1][0],a[0][0] * b[0][1] + a[0][1]*b[1][1]},
            {a[1][0] * b[0][0] + a[1][1]*b[1][0],a[1][0] * b[0][1] + a[1][1]*b[1][1]}
    };
}

void rozloz(int m, int a, std::vector<std::vector<std::vector<int>>> &rozklad){
    if (a%m != 0) {
        std::vector<std::vector<int>> tmp = {
                {m/a,1},
                {1,0}
        };

        rozklad.push_back(inverse_matrix(tmp));
        rozloz(a, m % a, rozklad);
    }
}

int euclidean(int m, int a){
    std::vector<std::vector<std::vector<int>>> rozklad;
    rozloz(m,a,rozklad);

    auto tmp = rozklad[0];

    for (int i=1; i<rozklad.size(); i++){
        tmp = multiply_matrices(tmp,rozklad[i]);
    }

    return tmp[1][0];
}

unsigned int gcd(unsigned int a,unsigned int b){
    if (a < b){
        std::swap(a,b);
    }
    if (b == 0){
        return a;
    }
    return gcd(b,a%b);
}

std::string encrypt(unsigned int a,unsigned int b,std::string &text){

    std::string result;

    for (char c: text){
        result += (char)(((a * (c-'a') + b) % MODULO)+'a');
    }

    return result;
}

std::string decrypt(unsigned int a,unsigned int b,std::string &text){
    std::string result;
    int ia = euclidean(MODULO, a);
    for (char c: text){
        result += (char)(((((((int)c)-'a'-b) * ia) % MODULO) + MODULO % MODULO) + 'a');
    }
    return result;
}

void print_help(char **argv){
    std::cout << "usage: " << argv[0] << " <-e|-d> <A> <B> [TEXT]" << std::endl;
    std::cout << "if TEXT is not specified, standard input is read." << std::endl;
}

int main(int argc, char **argv)
{
    int mode = 0;
    {
        std::string arg = argv[1];
        if (arg != "-e" && arg != "-d"){
            std::cerr << "Error: first parameter must be \"-e\" or \"-d\"! (not \"" << arg << "\")" << std::endl;
            print_help(argv);
            return 1;
        }
        else{
            if (arg == "-e"){
                mode = 1;
            }
            else if (arg == "-d"){
                mode = 2;
            }
        }
    }

    std::string text = "";
    unsigned int a;
    unsigned int b;

    if (argc < 4) {
        print_help(argv);
        return 2;
    }
        else if (argc < 5){
        std::stringstream buf;
        buf << std::cin.rdbuf();
        text = buf.str();
    }
    else {
        text = argv[4];
    }

    a = atoi(argv[2]);
    b = atoi(argv[3]);

    b = b % MODULO;

    if (gcd(a,MODULO) != 1){
        std::cerr << "gcd(a, 256) != 1 ---> aborting" << std::endl;
        return 3;
    }

    /*
    {
        auto it = text.begin();


        // spracovanie textu
        while(it != text.end()){
            if (*it >= 'A' && *it <= 'Z'){
                *it = *it - 'A' + 'a';
            }

            if (*it < 'a' || *it > 'z'){
                text.erase(it,it+1);
            }
            else {
                it++;
            }
        }
    }
    */

    // zasifrovanie
    if (mode == 1){
        std::cout << encrypt(a,b,text) << std::endl;
    }
    else if (mode == 2){
        std::cout << decrypt(a,b,text) << std::endl;
    }

    return 0;
}